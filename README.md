# README #

Simple spring boot app which uses RxJava. 

This applications translate 1-5 into two different language. 
For each number it make request to a chuck noris joke service.

The files can be found in src/main/java/com/demo

The application has 3 endpoints:
- http://localhost:8080/xrebel
- http://localhost:8080/v1
- http://localhost:8080/v2

v1 endpoint is example of how we convert the rxJava call to a future