package com.demo;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.demo.services.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import rx.Observable;

@RestController
public class DemoController {

    @Autowired
    private DemoService demoService;
    
    @GetMapping(value = "/v1", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DemoResult> v1() throws InterruptedException, ExecutionException, TimeoutException {
        return demoService.getDemos()
                          .toList()
                          .toBlocking()
                          .toFuture()
                          .get(10, TimeUnit.SECONDS);

    }
//
//    @GetMapping(value = "/v2", produces = MediaType.APPLICATION_JSON_VALUE)
//    public Observable<DemoResult> v2() {
//        return demoService.getDemos();
//    }
    
}
