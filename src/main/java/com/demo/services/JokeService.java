package com.demo.services;

import com.mashape.unirest.http.Unirest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class JokeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JokeService.class);

    public String getRandomJoke() {
        try {

            String result =  Unirest.get("https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random")
                                    .header("x-rapidapi-host", "matchilling-chuck-norris-jokes-v1.p.rapidapi.com")
                                    .header("x-rapidapi-key", "3c274d31dbmsh82e6b4ddb3a5578p19f006jsn8167326af01e")
                                    .header("accept", "application/json")
                                    .asJson()
                                    .getBody()
                                    .getObject()
                                    .getString("value");
            LOGGER.debug(Thread.currentThread().getName()+":random: "+result);
            return result;
        } catch (Exception e) {
            LOGGER.error("Unable to send message", e);
            return null;
        }
    }
}
