package com.demo.services;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class NumberTranslationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoService.class);

    private Map<Integer, String> en = ImmutableMap.of(
            1, "one",
            2, "two",
            3, "three",
            4, "four",
            5, "five"
    );

    private Map<Integer, String> fr = ImmutableMap.of(
            1, "un",
            2, "deux",
            3, "trois",
            4, "quatre",
            5, "cinq"
    );

    public String getEnglish(Integer number) {
        String result =  en.get(number);
        LOGGER.debug(Thread.currentThread().getName()+": english for "+number+": "+result);
        return result;
    }

    public String getFrench(Integer number) {
        String result =  fr.get(number);
        LOGGER.debug(Thread.currentThread().getName()+":français for "+number+": "+result);
        return result;
    }
}
