package com.demo.services;

import com.demo.DemoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;
import rx.schedulers.Schedulers;

@Service
public class DemoService {

    @Autowired
    private NumberTranslationService translationService;

    @Autowired
    private JokeService jokeService;

    public Observable<DemoResult> getDemos() {
        return Observable.range(0, 6)
                .map(num -> {
                    DemoResult result = new DemoResult();
                    result.number = num;
                    return result;
                })
                .map(demoResult -> {
                    demoResult.english = translationService.getEnglish(demoResult.number);
                    return demoResult;
                })
                .map(demoResult -> {
                    demoResult.français = translationService.getFrench(demoResult.number);
                    return demoResult;
                })
                .map(demoResult -> {
                    demoResult.also = jokeService.getRandomJoke();
                    return demoResult;
                })
                .subscribeOn(Schedulers.io()); // causes to execute in another thread
    }
}
